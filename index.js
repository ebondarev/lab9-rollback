const {Client} = require('pg')
const client = new Client({
    //user: 'dbuser',
    //host: 'database.server.com',
    //password: 'secretpassword',
    //port: 3211,
    database: 'company'
})

async function addSalaries(client, ids, employee_ids, amount) {
    await client.query('BEGIN')

    for (let i = 0; i < Math.min(ids.length, employee_ids.length, amount.length); i++) {
        const result = await client.query(`SELECT id FROM Salary WHERE employee_id=$1 or id=$2 LIMIT 1;`, [employee_ids[i], ids[i]])
        if (result.rows.length === 0) {
            try {
                await client.query(`INSERT INTO Salary VALUES ($1, $2, $3);`, [ids[i], employee_ids[i], amount[i]])
            } catch (e) {
                console.error(e.message)
                console.log('Rollback...')
                await client.query("ROLLBACK");
                await client.query("COMMIT");

                return false
            }
        }
    }

    await client.query("COMMIT");
    console.log('Done')
    return true
}

async function main(){
    await client.connect()

    await addSalaries(client,[1, 2], [1, 2, 3], [100, 100, 100])
    await addSalaries(client,[3, 4], [3, 4], [100, 'wrong'])

    //  RESULTS
    // id | employee_id | amount
    // ----+-------------+--------
    //  1 |           1 |    100
    //  2 |           2 |    100


    await client.end()
}

main()
